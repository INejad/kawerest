<?php

namespace Aviatoo\Rest\Exception;


use Aviatoo\Rest\Exception\Base\ApiException;

/**
 * Class AccessDeniedException
 * @package Aviatoo\Rest\Exception
 */
class AccessDeniedException extends ApiException
{
    /**
     * AccessDeniedException constructor.
     * @param array $errorData
     */
    public function __construct(array $errorData=[])
    {
        parent::__construct(403, $errorData, "Access Denied for User");
    }

}