<?php
namespace Aviatoo\Rest\Exception\JWT;
use Aviatoo\Rest\Exception\Base\ApiException;

/**
 * Class JWTInvalidException
 * @package Aviatoo\Rest\Exception\JWT
 */
class JWTInvalidException extends ApiException
{

    const MESSAGE = 'Invalid JWT Token';
    const STATUS_CODE = 401;

    /**
     * InvalidJWTException constructor.
     */
    public function __construct() {
        parent::__construct(self::STATUS_CODE, [],self::MESSAGE);
    }
}
