<?php
namespace Aviatoo\Rest\Exception\JWT;
use Aviatoo\Rest\Exception\Base\ApiException;

/**
 * Class JWTNotFoundException
 * @package Aviatoo\Rest\Exception\JWT
 */
class JWTNotFoundException extends ApiException
{
    const MESSAGE = 'JWT Token not found';
    const STATUS_CODE = 401;

    /**
     * JWTNotFoundException constructor.
     */
    public function __construct() {
        parent::__construct(self::STATUS_CODE, [],self::MESSAGE);
    }
}
