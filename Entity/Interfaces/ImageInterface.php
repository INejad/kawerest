<?php


namespace Aviatoo\Rest\Entity\Interfaces;

use Symfony\Component\HttpFoundation\File\File;

/**
 * Interface ImageInterface
 * @package Aviatoo\Rest\Entity\Interfaces
 */
interface ImageInterface
{

    /**
     * @return string
     */
    public function getName();

    /**
     * @return string
     */
    public function getPath();

    /**
     * @return File
     */
    public function getFile();

}